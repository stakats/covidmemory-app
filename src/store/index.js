import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import settings from '@/store/settings';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    settings,
  },
  plugins: [createPersistedState({
    key: 'covidmemory',
    paths: [
      'settings.language',
      'settings.cookiesAccepted',
    ],
  })],
});
