import axios from 'axios';
import Memory from '@/models/Memory';
import Media from '@/models/Media';

const VUE_APP_API_BASE_URL = process.env.VUE_APP_API_BASE_URL || '/omeka-s/api';
console.info('API_BASE_URL', VUE_APP_API_BASE_URL);

const request = axios.create({
  baseURL: VUE_APP_API_BASE_URL,
  timeout: 1000,
});

export const items = {
  async find() {
    const relatedMedia = await request.get('/media')
      .then(({ data }) => {
        const idx = {};
        data.forEach((m) => {
          idx[m['o:id']] = m;
        });
        return idx;
      });
    const its = await request.get('/items', {
      params: {
        sort_order: 'desc',
      },
    })
      .then(({ data }) => data.map((d) => ({
        ...d,
        'o:media': d['o:media'].map((m) => ({
          ...m,
          ...relatedMedia[m['o:id']],
        })),
      })));
    return {
      total: its.length,
      data: its,
    };
  },
  get(id) {
    return Promise.all([
      // get media objects connected to item
      request.get('/media', {
        params: {
          item_id: id,
        },
      })
        .then(({ data }) => data.map((d) => Media.create(d))),
      // get item
      request.get(`/items/${id}`)
        .then(({ data }) => Memory.create(data)),
    ]).then(([media, memory]) => {
      memory.setMedia(media);
      return memory;
    });
  },
};

export const media = {
  find() {
    return request.get('/media');
  },
};
