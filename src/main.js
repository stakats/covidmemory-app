import Vue from 'vue';
import { VueReCaptcha } from 'vue-recaptcha-v3';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import VueI18n from 'vue-i18n';
import VueRellax from 'vue-rellax';
import VueGtag from 'vue-gtag';
import VueHead from 'vue-head';

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import messages from '@/i18n/messages';
import dateTimeFormats from '@/i18n/dateTimeFormats';

Vue.config.productionTip = false;

Vue.use(VueReCaptcha, { siteKey: '6LdOKeUUAAAAANzimjwebCeF-OWBfxpLodETfL6V' });
// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueI18n);
Vue.use(VueRellax);
Vue.use(VueHead);

if (process.env.VUE_APP_GA_TRACKING_ID) {
  console.info('track id', process.env.VUE_APP_GA_TRACKING_ID);
  Vue.use(VueGtag, {
    config: {
      id: process.env.VUE_APP_GA_TRACKING_ID,
    },
  }, router);
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  fallbackLocale: 'en',
  locale: store.state.settings.language,
  messages,
  dateTimeFormats,
  silentTranslationWarn: true, // setting this to `true` hides warn messages about translation keys.
});

window.app = new Vue({
  i18n,
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

console.info('Current language:', store.state.settings.language);
