import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from '../views/Index.vue';

Vue.use(VueRouter);
console.log('public path', process.env.BASE_URL);
const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
  },
  {
    path: '/introduction',
    name: 'Intro',
    component: () => import(/* webpackChunkName: "about" */ '../views/Intro.vue'),
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/memories',
    name: 'Memories',
    component: () => import(/* webpackChunkName: "memories" */ '../views/Memories.vue'),
  },
  {
    path: '/memory/:id',
    name: 'Memory',
    component: () => import(/* webpackChunkName: "memories" */ '../views/Memory.vue'),
  },
  {
    path: '/new',
    name: 'Create',
    component: () => import(/* webpackChunkName: "create" */ '../views/Create.vue'),
  },
  {
    path: '/terms',
    name: 'Terms',
    component: () => import(/* webpackChunkName: "terms" */ '../views/Terms.vue'),
  },
  {
    path: '/privacy',
    name: 'Privacy',
    component: () => import(/* webpackChunkName: "terms" */ '../views/Privacy.vue'),
  },
  {
    path: '*',
    component: () => import(/* webpackChunkName: "terms" */ '../views/NotFound.vue'),
  },
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  mode: 'history',
  routes,
});

export default router;
