import Media from '@/models/Media';
import { getValue, COLORS } from '@/models/utils';


export default class Memory {
  constructor({
    id = 0,
    title = '',
    content = '',
    creator = '',
    email = '',
    pattern = [],
    date = new Date(),
    dateCreated = new Date(),
    dateModified = new Date(),
    media = [],
    place = '',
  } = {}) {
    this.id = parseInt(id, 10);
    this.title = String(title);
    this.email = String(email);
    this.content = String(content).trim();
    this.creator = String(creator).trim();
    this.place = String(place).trim();
    this.pattern = pattern;
    this.date = date;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
    this.scale = 1.5 * Math.random() + 1;
    this.color = COLORS[(this.id % (COLORS.length - 1))];
    this.media = media;
  }

  static create(item) {
    console.info('Memory.create', item);
    return new Memory({
      id: item['o:id'],
      title: item['o:title'],
      content: getValue(item, 'dcterms:description'),
      dateCreated: new Date(getValue(item, 'o:created')),
      dateModified: new Date(getValue(item, 'o:modified')),
      date: new Date(getValue(item, 'dcterms:created')),
      creator: getValue(item, 'dcterms:creator'),
      place: getValue(item, 'dcterms:spatial'),
      media: item['o:media'].map((m) => Media.create(m)),
    });
  }

  getDate() {
    let date = new Date();
    if (this.date instanceof Date && !Number.isNaN(this.date.getTime())) {
      date = this.date;
    } else if (this.dateCreated instanceof Date && !Number.isNaN(this.dateCreated.getTime())) {
      date = this.dateCreated;
    }
    return date;
  }

  getFormattedDate() {
    return this.getDate().toISOString().split('T')[0];
  }

  getFirstMedia() {
    if (!this.media.length) {
      return new Media();
    }
    return this.media[this.media.length - 1];
  }

  getThumbnail(size = 'square') {
    if (!this.media.length) {
      return '';
    }
    return this.media[this.media.length - 1].thumbnailUrls[size];
  }

  hasCreator() {
    return this.creator.length > 0;
  }

  hasPlace() {
    return this.place.length > 0;
  }

  setMedia(media = []) {
    this.media = media;
  }
}
