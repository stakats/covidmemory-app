import ThumbnailUrls from '@/models/ThumbnailUrls';
import { getValue } from '@/models/utils';

export default class Media {
  constructor({
    id = 0,
    mimetype = '', // 'video/quicktime',
    dateCreated = new Date(),
    dateModified = new Date(),
    thumbnailUrls = new ThumbnailUrls(),
    originalUrl = '',
  } = {}) {
    this.id = parseInt(id, 10);
    this.mimetype = String(mimetype);
    this.type = this.mimetype.split('/').shift();
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
    this.thumbnailUrls = thumbnailUrls;
    this.originalUrl = String(originalUrl);
  }

  static create(media) {
    console.info('Media.create', media);
    return new Media({
      id: media['o:id'],
      mimetype: media['o:media_type'],
      dateCreated: new Date(getValue(media, 'o:created')),
      dateModified: new Date(getValue(media, 'o:modified')),
      thumbnailUrls: new ThumbnailUrls(media['o:thumbnail_urls']),
      originalUrl: media['o:original_url'],
    });
  }

  getImageUrl() {
    return this.originalUrl;
  }
}
